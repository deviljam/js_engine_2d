//♡
i18n_language = "english";
i18n_messages = {
	"intro_text" : {
		"english" : "This is a closed alpha released on the 19th of June, 2016. This version is to be shared with close friends for a 'first impression' and to gather some feedback about the game. Is it too hard, too easy, no fun, too fun? Is there a part you found frustrating or something you think needs changed? This is what this demo is meant to assess. If you weren't explictly given this demo, then someone was naughty.",
		"engrish" : "Distant war has hurt the land. The people will search for their home to a new land is safe from a distance dispute. Look out for poverty. In the castle of a mysterious Beast Lords take what is necessary for what is needed to survive. You will save the new home."
	},
	"introduction" : {
		"english" : "Intro",
		"engrish" : "Learning"
	},
	"new_game" : {
		"english" : "New game",
		"engrish" : "Game new"
	},
	"press_start" : {
		"english" : "Press start",
		"engrish" : "Start button"
	},
	"introduction_help" : {
		"english" : "For developers only.",
		"engrish" : "You will learn How to play. Please enjoy to the story of origin."
	},
	"start_help" : {
		"english" : "Enter the world of Beast Lords. Play the closed alpha demo.",
		"engrish" : "Play the game. Please note, death is permanent."
	},
	"templenames" : {
		"english" : ["Anahilt Fortress","The Gardens of Benburb", "Carncastle", "Dunore Keep", "Edenmore Temple", "Foyal Palace"],
		"engrish" : ["Anahilt Fortress","Benburb Gardens", "Carncastle", "Dunore Keep", "Edenmore Temple", "Foyal Palace"]
	},
	"map_unknown" : {
		"english" : "Unknown",
		"engrish" : "Nowhere."
	},
	"maps" : {
		"english" : {
			"gateway.tmx"	:"Gateway", 
			"townhub.tmx"	:"Town",
			"firepits.tmx"	:"Furnace",
			"fridge.tmx"	:"Cooler",
			"temple1.tmx"	:"Palace",
			"temple2.tmx"	:"Anglemyer",
			"temple3.tmx"	:"Sanctuary",
			"temple4.tmx"	:"Residence"
		},
	},
	"maps_full" : {
		"engrish" : {
			"gateway.tmx"	:"Beast City Gateway", 
			"townhub.tmx"	:"Kinallen Town",
			"firepits.tmx"	:"Krafla Furnace",
			"fridge.tmx"	:"Grand Cooler",
			"temple1.tmx"	:"Chort's Palace",
			"temple2.tmx"	:"Anglemyer Tower",
			"temple3.tmx"	:"Rainer Sanctuary",
			"temple4.tmx"	:"Beast City Residence"
		},
	},
	"mayor_intro" : {
		"english" : [
			"Hello. I'm the Mayor of our town. Life is hard here.",
			"If we all work together we can make this a better place to live.",
			"Truth is... I have no idea what I'm doing.",
			"You look like a smart guy, maybe you can help.",
			"If you speak to me you can assign people to different projects",
			"Projects will cost money, the chancellor handles that."
		],
		"engrish" : [
			"Hello. My name is mayor.",
			"Help me this town better.",
			"I know nothing.",
			"You are smart.",
			"Press people to other construction.",
			"Donate to make the construction into a new with chancellor."
		]
	},
	"chancellor_howmuch" :{
		"english" : "How much would you like to donate?",
		"engrish" : "Money is of no object.",
	},
	"chancellor_intro" : {
		"english" : "I'm the chancellor of this town. I manage the money. It turns out I don't manage it very well at all. Say, you wouldn't want to donate a little to our good town? I promise, every single penny will go to good projects!",
		"engrish" : "My name is Chancellor. I make good with the money. The money is trouble. You can donate your money to the town through me. I'll spend your money correctly. Press people to other construction. Donate to make the construction into a new with my assistant."
	},
	"npc_chargeattackman" : {
		"english" : "Hold down the attack button with %fire% to unlease a powerful slash!"
	},
	"npc_dodgeman" : {
		"english" : "Did you know you can perform a dodge roll by pressing %dodge%?"
	},
	"npc_downstabman" : {
		"english" : "Did you know you can perform a downstab while jumping? Just hold down while you are in the air"
	},
	"smith_intro" : {
		"english" : "You there. Did you know you can only hold one weapon at a time? Don't worry, any weapon you leave behind I'll store it here for you. It'll be free of charge, because I'm kind like that."
	},
	"builder0" : {
		"english" : "We're just just gettin' started on this one, buddy.",
		"engrish" : "Play the game. Please note, death is permanent."
	},
	"builder1" : {
		"english" : "It's lookin' good. We'll be done in no time.",
		"engrish" : "The structure is half way complete."
	},
	"builder2" : {
		"english" : "We're nearly done building this one, buddy.",
		"engrish" : "We will complete this structure in short time."
	},
	"building_names" : {
		"english" : {
			"hall" : "Town hall",
			"mine" : "Gold mine",
			"lab" : "Wizard laboratory",
			"hunter" : "Hunter's shack",
			"mill" : "Wheat mill",
			"library" : "Library",
			"inn" : "Halfway house",
			"farm" : "Farm",
			"smith" : "Black smith",
			"bank" : "Bank"
		},
		"engrish" : {
			"hall" : "Town hall",
			"mine" : "Mine",
			"lab" : "Laboratory",
			"hunter" : "Bounty",
			"mill" : "Mill",
			"library" : "Library",
			"inn" : "Inn",
			"farm" : "Farm",
			"smith" : "Black smith",
			"bank" : "Bank"
		}
	},
	"questcomplete" : {
		"english" : "Quest complete!",
		"engrish" : "Quest complete!"
	},
	"quest" : {
		"english" : {
			"q0" : ["The blocked caves", "Talk to the professor in NEARBYTOWN.", "Speak with the professor's brother hidden in the woods south of NEARBYTOWN.","Use the wand to open the cave entrence"],
			"q1" : ["Reach the dead island", "Place your head against the Wailing Wall near Irata Mountain.", "Use the prayer to calm the resentful spirit."],
			"q2" : ["Release the lost souls", "Find the resting places and pray for their souls."],
			"q3" : ["Reach Doite", "Speak with someone in Irata village.", ""]
		},
		"engrish" : {
			"q0" : ["The blocked caves", "Talk to the professor in NEARBYTOWN.", "Speak with the professor's brother hidden in the woods south of NEARBYTOWN.","Use the wand to open the cave entrence"],
			"q1" : ["Reach the dead island", "Place your head against the Wailing Wall near Irata Mountain.", "Use the prayer to calm the resentful spirit."],
			"q2" : ["Release the lost souls", "Find the resting places and pray for their souls.","Return to the resentful spirit"],
			"q3" : ["Do the other thing", "Search for the wrecked ship off of Irata's east coast."]
		}
	},
	"greetings" : {
		"english" : [
			"Hello, stranger.",
			"Evening, friend"
		]
	},
	"miner0" : {
		"english" : [
			"This is no good. We were sent up here to mine. But we found this big old relief in the way. We don't exactly wanna break it. Could you ask the professor for us? He'll know what to do.",
			"Talk to the professor in NEARBYTOWN. He'll help us move this relief",
			"You talked to him? He wants you to get a magic wand? Maybe the professor is losing his marbles.",
			"I can't believe that actually worked. Here was me thinkin' we had the week off"
		]	
	},
	"town01_professor" :{
		"english" : [
			"Thank you, young... man. Without Chort's minons running around the countryside, my research can continue unabated!",
			"There's a large relief blocking the cave's entrence?! How amazing! That's a ancient door. Rather than have those brutes ruin it with their picks, find my brother in the south of the forest. He hides himself away, but he'll have the wand needed to open this cave.",
			"Have you spoken with my brother? He lives south of here, on the other side of the forest.",
			"You got the wand! It's certainly a marvel to see. It must be thousands of years old. Use it on the relief at the cave's entrence.",
			"Hard to believe after so many years, these ancient gadgets still work."
		]
	},
	"town02_hermit" :{
		"english" : [
			"Get out of here you wild thing!",
			"I'm sorry, I thought you were some wild creature looking for food. My brother sent you? He should know better than that. If it's the wand you're after here it is. But take good care of it. It's priceless.",
			"Next time your see my brother, tell him not to send anymore people to me. I just want to be left alone."
		]
	},
	"southcitymadman" : {
		"english" : [
			"You're trying to get to the island? There's a place next to Irata Mountain called the Wailing wall. I hear if you place your head against it, a spirit will take you to the island.",
			"Have you tried it yet? The Wailing Wall is just north of here, next to the foot of the mountain."
		]
	},
	"wailingwall" :{
		"english" : [
			"Are... are you okay there, friend?",
			"Someone told you if you put your head against this rock and you'll end up in across the river?! I think someone might be having you on. Here. Come and join us by our fire.",
			"My name is Lance, and this is my friend Carl. We're travelers in these parts. We don't see many people in these parts.",
			"Yeah, especially Beasts.",
			"Don't be rude, Carl.",
			"I'm not being rude. I'm just saying... I've nothing against Beast Lords.",
			"Some of your best friends are Beast Lords. Eh, Carl?",
			"As a matter of fact, yes.",
			"Carl is sweet on this Beast Lord girl, but she doesn't speak a word of the language.",
			"People can have a very meaningful time together without actually talking, Lance.",
			"Ooooh, look at you, boasting!",
			"Knock it off, Lance. She's not like that. She's really sweet.",
			"It's a good thing you two can't speak. She'd realise what an oaf you are.",
			"Anyway, why do you want to cross the river for, stranger? Haven't you heard the land there is haunted?",
			"He's not joking either. I heard there was a resentful spirit that strikes any trespassers dead with fright.",
			"I think Carl's girl may be from that island. The only words in our language she can speak is this weird little prayer. How does it go, Carl?",
			"I dunno if I should be telling you this. You seem friendly, the last thing I want is you rushing off into that acursed island...",
			"...but if you really want to know it...",
			"Good luck, friend. Don't mess with devils and ghosts."
		]
	},
	
	
	//Phantom Pass
	"phantompass" :{
		"english" : [
			"You're a strange one. You know the lament of the dead? It's a prayer for releasing lost souls, not many Mortals know it.",
			"You want past? I cannot allow it. This is the land of the dead. No living creature can be permitted.",
			"Perhaps there is a way for you to pass. There is something you can do for the dead to ease their weary souls. How about it?",
			"This is Bardo, the land between life and death. Find the lost souls in this place and pray for their release.",
			"I first wandered these lands, like you, as a living man. I wanted to help the ill and dying of this land. They were sent here to die, and offered respite.",
			"But there were so many dead here, I haven't even half completed my task.",
			"Good luck, young warrior. Return here once you have prayed for all the lost souls."
		]
	},
	"phantomend" :{
		"english" : [
			"Poor sorry beast. My duty here has lasted centuries, but at last it is coming to an end. I'm sorry it had to be you, but I've dreamt of this day since I first took up the reins.",
			"What do you think you're doing, Phantom?"
		]
	},
};
function i18n(name,replace){
	replace = replace || {};
	var out = "";
	if( name in i18n_messages ){
		if( i18n_language in i18n_messages[name] ){
			out = i18n_messages[name][i18n_language];
		}else {
			for(var i in i18n_messages[name]){
				out = i18n_messages[name][i];
				break;
			}
		}
	}
	for(var i in replace){
		out = out.replace(i, replace[i]);
	}
	return out;
}